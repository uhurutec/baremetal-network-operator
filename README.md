# The uMetal baremetal network operator

## Developing locally

Obtaining appropriate go version using local go installation:

```sh
GOBIN="${HOME}/.local/bin" go install golang.org/dl/go1.20@latest
go1.20 download
```

Building go sources:

```sh
make GO=go1.20 build
```

Building docker image against local docker installation:

```sh
make GO=go1.20 docker-build
```

Updating manifests and boilerplate after changes:

```sh
make GO=go1.20 manifests generate
```

Running tests:

```sh
make GO=go1.20 test
```


## Using a running management host to deploy and test operator

Provide image:

```sh
make GO=go1.20 docker-export | ssh -J ubuntu@umetal.kvm.corp umetal@10.42.0.50 'sudo k3s ctr images import -'
```

Install manifests:

```sh
make GO=go1.20 manifest-export | ssh -J ubuntu@umetal.kvm.corp umetal@10.42.0.50 'kubectl apply -f -'
```

To inspect what is going, you may e.g. open in interactive session on management host:

```sh
ssh -t -J ubuntu@umetal.kvm.corp umetal@10.42.0.50 'cd baremetal-network-operator && exec -a -bash bash'
```


## Prepare a release

A release consists of a tagged revision plus a corresponding image.

Adjust sources and tag revision:

```sh
make GO=go1.20 release VERSION=v…
```

The image is built by CI:

```sh
git push && git push --tags
```


## Obtain a release

Released manifests can be retrieved using *kustomize*, e.g. for release *v0.0.2*:

```sh
kubectl kustomize https://gitlab.com/uhurutec/baremetal-network-operator.git/config/crd?ref=v0.0.2
kubectl kustomize https://gitlab.com/uhurutec/baremetal-network-operator.git/config/default?ref=v0.0.2
```
