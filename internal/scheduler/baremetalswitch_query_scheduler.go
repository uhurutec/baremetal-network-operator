package scheduler

import (
	"crypto/tls"
	"fmt"
	"github.com/go-logr/logr"
	networkv1alpha1 "gitlab.com/uhurutec/baremetal-network-operator/api/v1alpha1"
	"io"
	"k8s.io/apimachinery/pkg/types"
	"net/http"
	"time"
)

const apiInfix = "restconf/data/sonic-vlan:sonic-vlan/"
const contentType = "application/yang-data+json"

type ApiResult struct {
	Result  string
	Message string
}

type QueryState string

const (
	QueryStateNone       QueryState = "none"
	QueryStateRunning    QueryState = "running"
	QueryStateTerminated QueryState = "terminated"
)

type BaremetalswitchQueryScheduler struct {
	log     logr.Logger
	queries map[types.NamespacedName]query
}

type query struct {
	resultChannel chan ApiResult
	timeout       time.Time
}

func NewBaremetalswitchQueryScheduler(log logr.Logger) BaremetalswitchQueryScheduler {
	return BaremetalswitchQueryScheduler{
		log:     log,
		queries: make(map[types.NamespacedName]query),
	}
}

func (scheduler BaremetalswitchQueryScheduler) IssueQuery(
	name types.NamespacedName,
	baremetalswitch networkv1alpha1.BaremetalswitchSpec,
	timeout time.Duration) {

	resultChannel := make(chan ApiResult)

	go func(scheduler BaremetalswitchQueryScheduler, baremetalswitch networkv1alpha1.BaremetalswitchSpec, resultChannel chan<- ApiResult) {
		resultChannel <- scheduler.apiQuery(baremetalswitch)
	}(scheduler, baremetalswitch, resultChannel)

	scheduler.queries[name] = query{resultChannel: resultChannel, timeout: time.Now().Add(timeout)}

}

func (scheduler BaremetalswitchQueryScheduler) apiQuery(baremetalswitch networkv1alpha1.BaremetalswitchSpec) ApiResult {

	url := baremetalswitch.Api + apiInfix
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		scheduler.log.Error(err, "unable to construct HTTP request")
		return ApiResult{Message: err.Error()}
	}

	request.Header.Set("accept", contentType)
	request.Header.Set("Content-Type", contentType)
	request.SetBasicAuth(baremetalswitch.Username, baremetalswitch.Password)
	transport := &http.Transport{ // TODO FIXME
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true}, // ignore expired SSL certificates
	}
	client := &http.Client{Transport: transport}
	scheduler.log.Info("querying", "url", url)
	response, err := client.Do(request)
	if err != nil {
		scheduler.log.Error(err, "HTTP request failed")
		return ApiResult{Message: err.Error()}
	}
	defer response.Body.Close()

	responseBody, err := io.ReadAll(response.Body)
	if err != nil {
		scheduler.log.Error(err, "HTTP response examination failed")
		return ApiResult{Message: err.Error()}
	}
	scheduler.log.Info("got response", "url", url)

	if response.StatusCode != http.StatusOK {
		scheduler.log.Info("bad response", "HTTP status code", response.StatusCode)
		return ApiResult{Message: fmt.Sprintf("Bad HTTP status code: %d – %s", response.StatusCode, responseBody)}
	}

	return ApiResult{Result: string(responseBody)}

}

func (scheduler BaremetalswitchQueryScheduler) ExamineQuery(name types.NamespacedName) (ApiResult, QueryState) {

	query, ok := scheduler.queries[name]
	if !ok {
		return ApiResult{}, QueryStateNone
	}

	select {
	case apiResult := <-query.resultChannel:
		scheduler.log.Info("query finished successfully")
		delete(scheduler.queries, name)
		return apiResult, QueryStateTerminated
	default:
		if time.Now().After(query.timeout) {
			scheduler.log.Info("query timed out")
			delete(scheduler.queries, name)
			return ApiResult{Message: "query timed out"}, QueryStateTerminated
		} else {
			scheduler.log.Info("query not finished yet")
			return ApiResult{}, QueryStateRunning
		}
	}
}
