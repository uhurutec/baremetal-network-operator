/*
Copyright (C) 2023 UhuruTec AG

This program is free software: you can redistribute it and/or modify it in
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package controller

import (
	"context"
	"gitlab.com/uhurutec/baremetal-network-operator/internal/scheduler"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"time"

	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	networkv1alpha1 "gitlab.com/uhurutec/baremetal-network-operator/api/v1alpha1"
)

var queryReconsideration = 5 * time.Second
var queryTimout = 3 * time.Minute
var queryRetryDelay = 5 * time.Minute
var queryReiterateDelay = 15 * time.Minute

// BaremetalswitchReconciler reconciles a Baremetalswitch object
type BaremetalswitchReconciler struct {
	client.Client
	Scheme *runtime.Scheme

	scheduler scheduler.BaremetalswitchQueryScheduler
}

//+kubebuilder:rbac:groups=network.umetal.io.uhurutec.com,resources=baremetalswitches,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=network.umetal.io.uhurutec.com,resources=baremetalswitches/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=network.umetal.io.uhurutec.com,resources=baremetalswitches/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.16.3/pkg/reconcile
func (r *BaremetalswitchReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	var log = log.FromContext(ctx)

	log.Info("baremetalswitch reconciliation")

	baremetalswitch := &networkv1alpha1.Baremetalswitch{}
	err := r.Get(ctx, req.NamespacedName, baremetalswitch)
	if err != nil {
		if apierrors.IsNotFound(err) {
			// If the custom resource is not found then, it usually means that it was deleted or not created
			// In this way, we will stop the reconciliation
			log.Info("baremetalswitch resource not found – ignoring since object must be deleted")
			return ctrl.Result{}, nil
		}
		// Error reading the object - requeue the request.
		log.Error(err, "failed to get baremetalswitch resource")
		return ctrl.Result{}, err
	}

	if baremetalswitch.Status.State == "" {
		baremetalswitch.Status.Configuration = ""
		baremetalswitch.Status.Message = "controller has taken notice of this switch"
		baremetalswitch.Status.State = networkv1alpha1.StateAcknowledged
		baremetalswitch.Status.UpdateTimestamp = metav1.Unix(0, 0)
		if err = r.Status().Update(ctx, baremetalswitch); err != nil {
			log.Error(err, "failed to update Baremetalswitch status")
			return ctrl.Result{}, err
		}
	}

	apiResult, queryState := r.scheduler.ExamineQuery(req.NamespacedName)

	if queryState == scheduler.QueryStateNone {
		if baremetalswitch.Status.State == networkv1alpha1.StateVital {
			now := time.Now()
			delay := baremetalswitch.Status.UpdateTimestamp.Add(queryReiterateDelay)
			if delay.After(now) {
				return ctrl.Result{RequeueAfter: delay.Sub(now)}, nil
			}
		}

		log.Info("initiating query for Baremetalswitch configuration")
		r.scheduler.IssueQuery(req.NamespacedName, baremetalswitch.Spec, queryTimout)
		return ctrl.Result{RequeueAfter: queryReconsideration}, nil
	}

	if queryState == scheduler.QueryStateRunning {
		return ctrl.Result{RequeueAfter: queryReconsideration}, nil
	}

	if apiResult.Message != "" {
		baremetalswitch.Status.Configuration = ""
		baremetalswitch.Status.Message = apiResult.Message
		baremetalswitch.Status.State = networkv1alpha1.StateFailed
		if err = r.Status().Update(ctx, baremetalswitch); err != nil {
			log.Error(err, "failed to update Baremetalswitch status")
			return ctrl.Result{}, err
		}
		return ctrl.Result{RequeueAfter: queryRetryDelay}, nil
	}

	baremetalswitch.Status.Configuration = apiResult.Result
	baremetalswitch.Status.Message = apiResult.Message
	baremetalswitch.Status.State = networkv1alpha1.StateVital
	baremetalswitch.Status.UpdateTimestamp = metav1.Now()
	if err = r.Status().Update(ctx, baremetalswitch); err != nil {
		log.Error(err, "failed to update Baremetalswitch status")
		return ctrl.Result{}, err
	}
	log.Info("successfully updated Baremetalswitch configuration")
	return ctrl.Result{RequeueAfter: queryReiterateDelay}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *BaremetalswitchReconciler) SetupWithManager(mgr ctrl.Manager) error {
	r.scheduler = scheduler.NewBaremetalswitchQueryScheduler(mgr.GetLogger())
	return ctrl.NewControllerManagedBy(mgr).
		For(&networkv1alpha1.Baremetalswitch{}).
		Complete(r)
}
