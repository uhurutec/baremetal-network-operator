/*
Copyright (C) 2023 UhuruTec AG

This program is free software: you can redistribute it and/or modify it in
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +kubebuilder:validation:Enum=acknowledged;failed;vital
type State string

const (
	StateAcknowledged State = "acknowledged"
	StateFailed       State = "failed"
	StateVital        State = "vital"
)

// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// BaremetalswitchSpec defines the desired state of Baremetalswitch
type BaremetalswitchSpec struct {
	Api      string `json:"api,omitempty"`
	Username string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
}

// BaremetalswitchStatus defines the observed state of Baremetalswitch
type BaremetalswitchStatus struct {
	State           State       `json:"state"`
	UpdateTimestamp metav1.Time `json:"updateTimestamp"`
	Configuration   string      `json:"configuration"`
	Message         string      `json:"message"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// Baremetalswitch is the Schema for the baremetalswitches API
// +kubebuilder:printcolumn:name="State",type="string",JSONPath=".status.state"
type Baremetalswitch struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   BaremetalswitchSpec   `json:"spec,omitempty"`
	Status BaremetalswitchStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// BaremetalswitchList contains a list of Baremetalswitch
type BaremetalswitchList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Baremetalswitch `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Baremetalswitch{}, &BaremetalswitchList{})
}
