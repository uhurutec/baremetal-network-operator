//go:build !ignore_autogenerated

/*
Copyright (C) 2023 UhuruTec AG

This program is free software: you can redistribute it and/or modify it in
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
*/

// Code generated by controller-gen. DO NOT EDIT.

package v1alpha1

import (
	runtime "k8s.io/apimachinery/pkg/runtime"
)

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *Baremetalswitch) DeepCopyInto(out *Baremetalswitch) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ObjectMeta.DeepCopyInto(&out.ObjectMeta)
	out.Spec = in.Spec
	in.Status.DeepCopyInto(&out.Status)
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new Baremetalswitch.
func (in *Baremetalswitch) DeepCopy() *Baremetalswitch {
	if in == nil {
		return nil
	}
	out := new(Baremetalswitch)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *Baremetalswitch) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *BaremetalswitchList) DeepCopyInto(out *BaremetalswitchList) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ListMeta.DeepCopyInto(&out.ListMeta)
	if in.Items != nil {
		in, out := &in.Items, &out.Items
		*out = make([]Baremetalswitch, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new BaremetalswitchList.
func (in *BaremetalswitchList) DeepCopy() *BaremetalswitchList {
	if in == nil {
		return nil
	}
	out := new(BaremetalswitchList)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *BaremetalswitchList) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *BaremetalswitchSpec) DeepCopyInto(out *BaremetalswitchSpec) {
	*out = *in
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new BaremetalswitchSpec.
func (in *BaremetalswitchSpec) DeepCopy() *BaremetalswitchSpec {
	if in == nil {
		return nil
	}
	out := new(BaremetalswitchSpec)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *BaremetalswitchStatus) DeepCopyInto(out *BaremetalswitchStatus) {
	*out = *in
	in.UpdateTimestamp.DeepCopyInto(&out.UpdateTimestamp)
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new BaremetalswitchStatus.
func (in *BaremetalswitchStatus) DeepCopy() *BaremetalswitchStatus {
	if in == nil {
		return nil
	}
	out := new(BaremetalswitchStatus)
	in.DeepCopyInto(out)
	return out
}
